FROM alpine:3.5

#
# ARG lets me import arguments given on the 'docker build' command line with '--build-arg'
# These ARGs are then available to use in the docker build context as variables
# ARGs don't end up in the image unless you put them there as LABELs or ENVs
#
ARG TAG=undefined
ARG BRANCH
ARG COMMIT_ID

#
# Use the ARGs to set LABELS which are baked into the image.
# You can see them with 'docker inspect'
#
# Nota Bene! Use namespacing (reverse DNS) to compartmentalize your labels.
# This is only needed on the LABELs, not on the ARGs or ENVs, because LABELs
# are for describing the _image_, they don't get passed to the runtime environment
#
LABEL gov.doe.jgi.TAG=$TAG \
      gov.doe.jgi.BRANCH=$BRANCH \
      gov.doe.jgi.COMMIT_ID=$COMMIT_ID

#
# We can set other labels too. We could have done this all on one line,
# continued from above, but I've separated it out for clarity
#
LABEL gov.doe.jgi.MAINTAINER="Tony Wildish wildish@lbl.gov" \
      gov.doe.jgi.EMAIL="wildish@lbl.gov" \
      gov.doe.jgi.APP_TYPE="assembler" \
      gov.doe.jgi.INPUT_DIR="/input" \
      gov.doe.jgi.OUTPUT_DIR="/output" \
      gov.doe.jgi.BASE_IMAGE="alpine:3.5"

#
# ENV let's me pass the ARGs from the build context into the runtime environment
#
ENV TAG=$TAG \
    BRANCH=$BRANCH \
    COMMIT_ID=$COMMIT_ID

#
# And when the container runs, it can inspect those values!
#
ADD metadata.sh /bin/metadata.sh
CMD /bin/metadata.sh
